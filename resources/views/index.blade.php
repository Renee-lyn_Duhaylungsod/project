@extends('layout')

@section('content')

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message}} </p>
    </div>
@endif
<div class="row">
    <div class="col-md-6">
        <h1> Laravel tests</h1>
    </div>
    <div class="col-md-4">
        <form action="/search" method="get"> 
            <div class="form-group">
                <input type="search" name="search" class="form-control">
                <span class="form-group-btn">
                    <button type="submit" class="btn btn-primary">Search</button>
                </span>
            </div>
        </form>
    
    </div>

    <div class="col-md-2 text-right">
        <a href="{{ action([App\Http\Controllers\PostController::class, 'create']) }}" class="btn btn-primary"> Create Post</a>
    </div>
</div>
<table class = "table table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Details</th>
            <th>Author</th>
            <th>Action</th>

        </tr>
    </thead>
    <tbody>
        @foreach($posts as $post)
            <tr>
                <td>{{ $post->id}}</td>
                <td>{{ $post->name}} </td>
                <td>{{ $post->details}} </td>
                <td>{{ $post->author}} </td>
                <td>
                    <form action="{{ action([App\Http\Controllers\PostController::class, 'destroy'], $post->id ) }}" method="post">
                        <a href="{{ action([App\Http\Controllers\PostController::class, 'show'], $post->id ) }}" class="btn btn-info"> show </a>
                        <a href="{{ action([App\Http\Controllers\PostController::class, 'edit'], $post->id ) }}" class="btn btn-warning"> edit </a>
                    

                        @csrf
                        @method('DELETE')
                        <button type=submit class="btn btn-danger"> delete</button>
                        
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>

</table>

@endsection